**Adds a dark theme to the Audible Website and Webplayer.**

The main focus and design is on the library page and the web player. If you have problems, just toggle the dark mode for your needs.

If you have suggestions or find bugs please report them in my gitlab repository [https://gitlab.com/Morice/audible-dark-theme](https://gitlab.com/Morice/audible-dark-theme).
